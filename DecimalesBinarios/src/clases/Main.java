package clases;

import java.util.Scanner;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 *
 * @author Febre
 */
public class Main {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        Algoritmos objConversion = new Algoritmos();

        /*
        System.out.println("Ingresa un nuero decimal");
        int numero = entrada.nextInt();

        System.out.println(objConversion.retornaDecimalBinario(numero));
        
         */
        // Otro algoritmo
        int valor = objConversion.retornaBinarioDecimal("11110");
        System.out.println(valor);
    }

}
