package clases;

/**
 *
 * @author Febre
 */
public class Algoritmos {

    public String retornaDecimalBinario(int numeroDecimal) {
        // Variables
        String contenedor = "";
        String binario = "";
        boolean salir = false;

        // Algoritmo que convierte de decimal a binario
        while (salir == false) {
            if (numeroDecimal % 2 == 0) {
                contenedor = contenedor + "0";

            }
            if (numeroDecimal % 2 == 1) {
                contenedor = contenedor + "1";
            }

            numeroDecimal = numeroDecimal / 2;

            if (numeroDecimal <= 0) {
                salir = true;
            }

        }// Fin de bucle

        // Algoritmo que ordena el resultado
        for (int i = contenedor.length(); i > 0; i--) {

            binario = binario + String.valueOf(contenedor.charAt(i - 1));

        }

        return binario;
    }//Fin del metodo

    public int retornaBinarioDecimal(String binario) {

        // Arreglos y variables contenedores
        int arregloA[] = new int[binario.length()];
        int arregloB[] = new int[binario.length()];
        int arregloC[] = new int[binario.length()];
        int numero = 0;
        int sumatoria = 0;

        // Bucle almacena en el primer arreglo
        for (int i = binario.length() - 1; i >= 0; i--) {

            String a = String.valueOf(binario.charAt(i));
            arregloA[i] = Integer.parseInt(a);
            // Limpio variable
            a = "";
        }

        for (int i = binario.length() - 1; i >= 0; i--) {

            arregloB[i] = (int) Math.pow(2, numero++);

        }

        for (int i = 0; i < arregloB.length; i++) {
            arregloC[i] = (arregloA[i] * arregloB[i]);
            sumatoria = sumatoria + arregloC[i];

        }

        // Cambiar
        return sumatoria;
    }

}
